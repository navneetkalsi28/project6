package com.skilltrace.model;

public enum Department {

	VCM, NNT, VES, GTS, RD;
}
