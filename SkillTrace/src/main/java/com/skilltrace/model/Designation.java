package com.skilltrace.model;

public enum Designation {

	DEVELOPER, MANAGER, SOFTWARE_ENGINEER, TESTER, DESIGNER, DELIVERY_HEAD;
}
