package com.skilltrace.model;

public enum Status {

	PENDING, APPROVED, REJECTED;
}
