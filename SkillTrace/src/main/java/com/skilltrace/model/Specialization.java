package com.skilltrace.model;

public enum Specialization {
	FRONT_END, BACK_END, MIDDLEWARE, DEVOPS, AUTOMATION, ARTIFICIAL_INTELLIGENCE;
}
