export class Skill {
    public skillId: number;
    public skillName: string;
    public score: number;
    public specialization: string;
}
