export class SkillRequest {
    public requestId: number;
    public employeeId: number;
    public employeeName: string;
    public skillRequestedId: number;
    public skillRequestedName: string;
    public status: string;
}
