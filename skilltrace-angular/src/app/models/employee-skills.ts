export class EmployeeSkills {
    public employeeSkillsId: number;
    public employeeId: number;
    public employeeName: string;
    public skillId: number;
    public skillName: string;
    public level: number;
}
