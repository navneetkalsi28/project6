import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(){}

  // first: string;
  // second: string;
  // logoUrl: string;
  // field: string;
  // srchValue: string;

  // constructor(
  //   private router: Router
  // ) {
  //   this.first = 'Skill';
  //   this.second = 'Trace';
  //   this.logoUrl = '/assets/images/ski.png';
  //   this.field = 'skillName';
  //   this.srchValue = '';
  // }

  // doSearch() {
  //   this.router.navigate(['/listSkills'], { queryParams: { field: this.field, srchValue: this.srchValue } });
  // }

  // doChangeField(field, ele) {
  //   this.field = field;
  //   this.srchValue = '';
  //   switch (field) {
  //     case 'skillName': ele.placeholder = 'Skill Name'; ele.type = 'text'; break;
  //     // case 'chargePerMonth': ele.placeholder = 'Charge Per Month'; ele.type = 'number'; break;
  //     // case 'maxUsage': ele.placeholder = 'Maximum Usage'; ele.type = 'number'; break;
  //   }
  // }

}
