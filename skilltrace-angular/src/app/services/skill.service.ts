import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Skill } from './../models/skill';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  baseUrl: string;
  constructor(private http: Http) {
    this.baseUrl = 'http://localhost:2828/skills';
  }

  getBaseUrlById(skillId: number): string {
    return this.baseUrl + '/' + skillId;
  }
  getSearchUrl(field: string, value: string): string {
    return this.baseUrl + '/' + field + '/' + value;
  }

  getJsonContentTypeHeader(): RequestOptions {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new RequestOptions({ headers: headers });
  }


  getAllSkills(): Observable<Skill[]> {
    return this.http.get(this.baseUrl).pipe(
      map(data => data.json())
    );
  }

  searchSkills(field: string, value: string): Observable<Skill[]> {
    return this.http.get(this.getSearchUrl(field, value)).pipe(
      map(data => data.json())
    );
  }

  getSkillById(skillId: number): Observable<Skill> {
    return this.http.get(this.getBaseUrlById(skillId)).pipe(
      map(data => data.json())
    );
  }

  deleteSkillById(skillId: number): Observable<Response> {
    return this.http.delete(this.getBaseUrlById(skillId));
  }

  addSkill(skill: Skill): Observable<Skill> {
    return this.http.post(this.baseUrl, JSON.stringify(skill),
      this.getJsonContentTypeHeader()).pipe(
        map(data => data.json())
      );
  }
  updateSkill(skill: Skill): Observable<Skill> {
    return this.http.put(this.baseUrl, JSON.stringify(skill),
      this.getJsonContentTypeHeader()).pipe(
        map(data => data.json())
      );
  }
}
