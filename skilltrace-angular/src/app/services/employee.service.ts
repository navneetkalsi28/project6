import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Employee } from './../models/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  baseUrl: string;
  constructor(private http: Http) {
    this.baseUrl = 'http://localhost:2828/employees';
  }

  getEmployeeByEmail(emailId: string): Observable<Employee[]> {
    return this.http.get(this.getSearchUrl('emailId', emailId)).pipe(
      map(data => data.json())
    );
  }

  getBaseUrlById(employeeId: number): string {
    return this.baseUrl + '/' + employeeId;
  }
  getSearchUrl(field: string, value: string): string {
    return this.baseUrl + '/' + field + '/' + value;
  }

  getJsonContentTypeHeader(): RequestOptions {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new RequestOptions({ headers: headers });
  }


  getAllEmployees(): Observable<Employee[]> {
    return this.http.get(this.baseUrl).pipe(
      map(data => data.json())
    );
  }

  searchEmployees(field: string, value: string): Observable<Employee[]> {
    return this.http.get(this.getSearchUrl(field, value)).pipe(
      map(data => data.json())
    );
  }

  getEmployeeById(employeeId: number): Observable<Employee> {
    return this.http.get(this.getBaseUrlById(employeeId)).pipe(
      map(data => data.json())
    );
  }

  deleteEmployeeById(employeeId: number): Observable<Response> {
    return this.http.delete(this.getBaseUrlById(employeeId));
  }

  addEmployee(employee: Employee): Observable<Employee> {
    return this.http.post(this.baseUrl, JSON.stringify(employee),
      this.getJsonContentTypeHeader()).pipe(
        map(data => data.json())
      );
  }
  updateEmployee(employee: Employee): Observable<Employee> {
    return this.http.put(this.baseUrl, JSON.stringify(employee),
      this.getJsonContentTypeHeader()).pipe(
        map(data => data.json())
      );
  }
}
