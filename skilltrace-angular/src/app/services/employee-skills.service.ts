import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EmployeeSkills } from './../models/employee-skills';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeSkillsService {

  baseUrl: string;
  constructor(private http: Http,
  private loginService: LoginService
  ) {
    this.baseUrl = 'http://localhost:2828/employeeSkills';
  }

  getBaseUrlById(employeeSkillsId: number): string {
    return this.baseUrl + '/' + employeeSkillsId;
  }
  getSearchUrl(field: string, value: string): string {
    return this.baseUrl + '/' + field + '/' + value;
  }

  getSearchUrlBySkillName(field: string, value: string): string {
    return this.baseUrl + '/' + this.loginService.getEmployeeId() + '/' + field + '/' + value;
  }

  getJsonContentTypeHeader(): RequestOptions {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new RequestOptions({ headers: headers });
  }


  getAllEmployeeSkills(): Observable<EmployeeSkills[]> {
    return this.http.get(this.baseUrl).pipe(
      map(data => data.json())
    );
  }

  searchEmployeeSkills(field: string, value: string): Observable<EmployeeSkills[]> {
    return this.http.get(this.getSearchUrl(field, value)).pipe(
      map(data => data.json())
    );
  }
  searchEmployeeSkillsByName(field: string, value: string): Observable<EmployeeSkills[]> {
    return this.http.get(this.getSearchUrlBySkillName(field, value)).pipe(
      map(data => data.json())
    );
  }

  getEmployeeSkillsById(employeeSkillsId: number): Observable<EmployeeSkills> {
    return this.http.get(this.getBaseUrlById(employeeSkillsId)).pipe(
      map(data => data.json())
    );
  }

  deleteEmployeeSkillsById(employeeSkillsId: number): Observable<Response> {
    return this.http.delete(this.getBaseUrlById(employeeSkillsId));
  }

  addEmployeeSkills(employeeSkill: EmployeeSkills): Observable<EmployeeSkills> {
    return this.http.post(this.baseUrl, JSON.stringify(employeeSkill),
      this.getJsonContentTypeHeader()).pipe(
        map(data => data.json())
      );
  }
  updateEmployeeSkills(employeeSkill: EmployeeSkills): Observable<EmployeeSkills> {
    return this.http.put(this.baseUrl, JSON.stringify(employeeSkill),
      this.getJsonContentTypeHeader()).pipe(
        map(data => data.json())
      );
  }
}
