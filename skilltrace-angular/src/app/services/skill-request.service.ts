import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SkillRequest } from './../models/skill-request';

@Injectable({
  providedIn: 'root'
})
export class SkillRequestService {

  baseUrl: string;
  constructor(private http: Http) {
    this.baseUrl = 'http://localhost:2828/skillRequests';
  }

  getBaseUrlById(skillRequestRequestId: number): string {
    return this.baseUrl + '/' + skillRequestRequestId;
  }
  getSearchUrl(field: string, value: string): string {
    return this.baseUrl + '/' + field + '/' + value;
  }

  getJsonContentTypeHeader(): RequestOptions {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new RequestOptions({ headers: headers });
  }


  getAllSkillRequests(): Observable<SkillRequest[]> {
    return this.http.get(this.baseUrl).pipe(
      map(data => data.json())
    );
  }

  searchSkillRequests(field: string, value: string): Observable<SkillRequest[]> {
    return this.http.get(this.getSearchUrl(field, value)).pipe(
      map(data => data.json())
    );
  }

  getSkillRequestById(skillRequestId: number): Observable<SkillRequest> {
    return this.http.get(this.getBaseUrlById(skillRequestId)).pipe(
      map(data => data.json())
    );
  }

  deleteSkillRequestById(skillRequestId: number): Observable<Response> {
    return this.http.delete(this.getBaseUrlById(skillRequestId));
  }

  addSkillRequest(skillRequest: SkillRequest): Observable<SkillRequest> {
    return this.http.post(this.baseUrl, JSON.stringify(skillRequest),
      this.getJsonContentTypeHeader()).pipe(
        map(data => data.json())
      );
  }
  updateSkillRequest(skillRequest: SkillRequest): Observable<SkillRequest> {
    return this.http.put(this.baseUrl, JSON.stringify(skillRequest),
      this.getJsonContentTypeHeader()).pipe(
        map(data => data.json())
      );
  }
}
