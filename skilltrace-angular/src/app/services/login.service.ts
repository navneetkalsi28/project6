import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  employeeId: number;
  employeeName: string;

  constructor() {
    this.employeeId = 222481;
    this.employeeName = 'Pablo Maffeo';
   }

  getEmployeeId(): number {
    return this.employeeId;
  }

  getEmployeeName(): string {
    return this.employeeName;
  }

}
