import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ListSkillsComponent } from './components/list-skills/list-skills.component';
import { AddSkillComponent } from './components/add-skill/add-skill.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ContactComponent } from './components/contact/contact.component';
import { NotificationComponent } from './components/notification/notification.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { RequestSkillComponent } from './components/request-skill/request-skill.component';
import { ViewReportComponent } from './components/view-report/view-report.component';
import { LoaderComponentComponent } from './components/loader-component/loader-component.component';
import { EmployeeHomeComponent } from './components/employee-home/employee-home.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'listSkills', component: ListSkillsComponent },
  { path: 'addSkill', component: AddSkillComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'editProfile', component: EditProfileComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'notification', component: NotificationComponent },
  { path: 'viewReport', component: ViewReportComponent },
  { path: 'requestSkill', component: RequestSkillComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ListSkillsComponent,
    AddSkillComponent,
    EditProfileComponent,
    LoginComponent,
    SignupComponent,
    ContactComponent,
    NotificationComponent,
    SidebarComponent,
    RequestSkillComponent,
    ViewReportComponent,
    LoaderComponentComponent,
    EmployeeHomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
